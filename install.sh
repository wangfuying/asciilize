#!/bin/bash
echo "欢迎使用二傻一键安装系统"
echo ""
echo "正在检查 Go 环境……"
brew install go
pwd
echo "准备为您安装 asciilize……"
echo "开始编译……"
go build asciilize.go
echo "开始安装……"
echo "正在请求 root 权限……"
echo "删除旧版本残留文件……"
sudo rm /usr/local/bin/asciilize
sudo ln \-s $(cd "$(dirname "$0")";pwd)/asciilize /usr/local/bin/asciilize
sudo chmod 777 /usr/local/bin/asciilize
echo ""
echo "程序安装完毕，切勿删除本目录"