# asciilize
用GO语言编写的将图片转换成ASCII字符的工具
 
## 参数 ：
* -f jpg/jpeg文件的路径  
* -bx 小块宽度  
* -by 小块高度  
* -j 取样精度（每n步取样一次）
 
## 使用方法：
```bash
./program -f ./in.jpg -j 1  
./program -f ./in.jpg -bx 20 -by 40 -j 400  
./program  
```

## Mac 傻瓜式安装
```bash
sudo chmod 777 ./install.sh
./install.sh
```
即可使用二傻安装器完成安装

## 效果
见 demo.txt